# GitLab Mirror

Synchronize all repos from [GitHub k8scat](https://github.com/k8scat) to [GitLab k8scat](https://github.com/k8scat-archived) using [action-mirror-git](https://github.com/k8scat/action-mirror-git).
